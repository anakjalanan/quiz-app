# README

**Step-by-step**

 - git clone https://bitbucket.org/anakjalanan/quiz-app.git
 - cd quiz-app && bundle
 - yarn --cwd client install
 - bin/rake db:migrate db:seed start

Once you're ready to deploy to Heroku, run:

 - heroku apps:create
 - heroku buildpacks:add heroku/nodejs --index 1
 - heroku buildpacks:add heroku/ruby --index 2
 - git push heroku master
 - heroku run rake db:seed
 - heroku open

To run backend test:

 - bundle exec rspec

To run frondent test:

 - yarn --cwd client test

CMS using Active Admin

 - URL : {domain}/admin/
 - Username : admin@example.com
 - Password : password

DEMO

 - Quiz App : https://thawing-island-12197.herokuapp.com
 - CMS : https://thawing-island-12197.herokuapp.com/admin