class ApiController < ActionController::API
  include Response
  include ExceptionHandler
  include ActionController::Serialization
end