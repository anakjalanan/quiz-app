class QuizSerializer < ActiveModel::Serializer
  attributes :id, :question, :answer, :answer_as_text

  def answer
    object.answer.to_s
  end

  def answer_as_text
    object.answer.to_words.gsub("-", " ")
  end
end
