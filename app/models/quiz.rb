class Quiz < ApplicationRecord
  validates_presence_of :question, :answer
end
