import React from 'react';
import ReactDOM from 'react-dom';
import { configure, shallow } from 'enzyme';
import { expect } from 'chai';
import App from './App';
import Quiz from './App';

import Adapter from 'enzyme-adapter-react-16'
configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

describe('App component testing', function() {
  const wrapper = shallow(<App />); 

  it('renders title message', function() {
    const title = <h1>Simple Quiz made with ReactJS</h1>;
    expect(wrapper.contains(title)).to.equal(true);
  });
})

describe('Quiz component testing', function() {
  const wrapper = shallow(<Quiz />);

  it('should have a form', function () {
    expect(wrapper.find('form')).to.exist;
  });

  it('should have an input', function () {
    expect(wrapper.find('form.input')).to.exist;
  });

  it('should have a submit button', function () {
    expect(wrapper.find('form.submit')).to.exist;
  });
})