import React from 'react';
import logo from './logo.svg';
import classNames from 'classnames';
import './App.css';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            q: [],
            counter: 0
        };

        this.incrementCounter = this.incrementCounter.bind(this);
    }
    
    render() {
        var data = "NO question created";
      if( this.state.q.length > 0 ){
        data =this.state.q.map((qa,index)=> {
          const counter = index+1;
          const quiz = this.state.q[index];
          return <Quiz key={counter} incrementCounter={this.incrementCounter} counter={this.state.counter} question={"Question "+counter+" : "+quiz.question} answerNumber={quiz.answer} answerText={quiz.answer_as_text}/>
        });
       }
       
        return (
            <div className="App">
              <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
            </header>
            <h1>Simple Quiz made with ReactJS</h1>
            <div><span>Correct answers: {this.state.counter} / {this.state.q.length}</span></div>
            {data}
          </div>
        );
    };

    fetch (endpoint) {
      return window.fetch(endpoint)
        .then(response => response.json())
        .catch(error => console.log(error))
    }

    componentDidMount () {
      this.getQuizzes()
    }
  
    getQuizzes () {
      this.fetch('/api/quizzes')
        .then(quizzes => {
          if (quizzes.length) {
            this.setState({q: quizzes})
          } else {
            this.setState({q: []})
          }
        })
    }

    incrementCounter() {
        var newCounter = this.state.counter + 1;
        this.setState({counter:newCounter})
    };

}

class Quiz extends React.Component {
    constructor(props){
      super(props);
      this.state = {
        userAnswer: "",
        isDisabled: "",
        className: "normal",
        buttonValue: "Check answer",
        placeholderText: "Your answer"
      }
      this.checkAnswer = this.checkAnswer.bind(this);
      this.typeAnswer = this.typeAnswer.bind(this);
    }

    typeAnswer(e) {
      this.setState({userAnswer:e.target.value});
    }

    checkAnswer (e) {
      e.preventDefault();
      var userAnswer = this.state.userAnswer.toLowerCase().trim();
      var answerNumber = this.props.answerNumber.toLowerCase().trim();
      var answerText = this.props.answerText.toLowerCase().trim();
      var isCorrect = (userAnswer === answerNumber || userAnswer === answerText) ? true : false;
      if (isCorrect) {
        this.props.incrementCounter();
        this.setState({isDisabled:"disabled"});
        this.setState({className:"correct animated rubberBand"});
        this.setState({buttonValue:"Correct answer!"});
      } else {
        this.setState({className:"incorrect animated shake"});
        this.setState({userAnswer:""});
        this.setState({placeholderText:"Try again"});
      }
    }

    render() {
        return (
            <form onSubmit={this.checkAnswer} className="set">
            <div dangerouslySetInnerHTML={{ __html: this.props.question }} />
            <div className="form-group">
                <input name="q" 
                className={classNames(this.state.className)} 
                placeholder={this.state.placeholderText}
                value={this.state.userAnswer}
                onChange={this.typeAnswer}
                disabled={(this.state.isDisabled)? "disabled" : ""}/>
            </div> 
            <input type="submit" 
              value={this.state.buttonValue} 
              disabled={(this.state.isDisabled)? "disabled" : ""} />
            
          </form>
        );
    };
}

export default App;
