class ChangeQuestionToBeTextInQuizzes < ActiveRecord::Migration[5.2]
  def change
    change_column :quizzes, :question, :text
  end
end
