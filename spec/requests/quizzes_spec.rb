require 'rails_helper'

RSpec.describe 'Quizzes API', type: :request do
  # initialize test data 
  let!(:quizzes) { create_list(:quiz, 10) }
  let(:quiz_id) { quizzes.first.id }

  # Test suite for GET /api/quizzes
  describe 'GET /api/quizzes' do
    # make HTTP get request before each example
    before { get '/api/quizzes' }

    it 'returns quizzes' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
    
    it 'returns the answer as string' do
      expect(json).not_to be_empty
      
      answer = quizzes.first.answer
      expect(json[0]['answer']).to eq(answer.to_s)
    end
    
    it 'returns the answer as words' do
      expect(json).not_to be_empty
      
      answer = quizzes.first.answer
      answer_as_text = answer.to_words.gsub("-", " ")
      expect(json[0]['answer_as_text']).to eq(answer_as_text)
    end
  end

  # Test suite for GET /api/quizzes/:id
  describe 'GET /api/quizzes/:id' do
    before { get "/api/quizzes/#{quiz_id}" }

    context 'when the record exists' do
      it 'returns the todo' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(quiz_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:quiz_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Quiz/)
      end
    end
  end

  # Test suite for POST /api/quizzes
  describe 'POST /api/quizzes' do
    # valid payload
    let(:valid_attributes) { { question: 'Fill 24', answer: '24' } }

    context 'when the request is valid' do
      before { post '/api/quizzes', params: valid_attributes }

      it 'creates a todo' do
        expect(json['question']).to eq('Fill 24')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/quizzes', params: { question: 'Fill 24' } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Answer can't be blank/)
      end
    end
  end

  # Test suite for PUT /api/quizzes/:id
  describe 'PUT /api/quizzes/:id' do
    let(:valid_attributes) { { title: 'Fill 10' } }

    context 'when the record exists' do
      before { put "/api/quizzes/#{quiz_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE /api/quizzes/:id
  describe 'DELETE /api/quizzes/:id' do
    before { delete "/api/quizzes/#{quiz_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end