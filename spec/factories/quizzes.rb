FactoryBot.define do
  factory :quiz do
    question { Faker::Lorem.word }
    answer { Faker::Number.number(10) }
  end
end